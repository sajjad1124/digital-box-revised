var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');

// Data Schema
var DataSchema = mongoose.Schema({
    DATETIME: {
        type: String,
        index: true
    },
    TEMPERATURE: {
        type: Number,
    },
    CO2: {
        type: Number,
    },
    VOC: {
        type: Number,
    },
    METHANE: {
        type: Number,
    }
});

var Data = module.exports = mongoose.model('Data', DataSchema);

