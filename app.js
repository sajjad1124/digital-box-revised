var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");

// imported libraries starts
const http = require("http");
const mongoose = require("mongoose");
const mongojs = require("mongojs");
const expressHandlebars = require("express-handlebars");
const methodOverride = require("method-override");
const session = require("express-session");
const passport = require("passport");
const localStrategy = require("passport-local").Strategy;
const expressValidator = require("express-validator");
const flash = require("connect-flash");
const mqtt = require("mqtt");
const ThingSpeakClient = require("thingspeakclient");
const bufferSplit = require("buffer-split");
const moment = require("moment");
const mysql = require("mysql");

var tsclient = new ThingSpeakClient();
// imported libraries ends

// mongoose Connection
mongoose.connect("mongodb://localhost/digitalbox");
const db = mongoose.Connection;

// mongojs connection
const dbs = mongojs("mongodb://localhost/digitalbox", ["sensordata"]);
const dbo = mongojs("mongodb://localhost/digitalbox", ["outliers"]);

var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");
var sensorsRouter = require("./routes/sensors");
var detailsRouter = require("./routes/details");
var graphsRouter = require("./routes/graph");
var forgotPasswordRouter = require("./routes/forgotPassword");
var zoneRouter = require("./routes/zone");
var mapRouter = require("./routes/map");

// initiating app
var app = express();
const server = http.createServer(app);

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.engine("handlebars", expressHandlebars({ defaultLayout: "layout" }));
app.set("view engine", "handlebars");

// middleware
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

// method override
app.use(methodOverride("_method"));

// set static folder
app.use(express.static(path.join(__dirname, "public")));

// Express Session
app.use(
  session({
    secret: "secret",
    saveUninitialized: true,
    resave: true
  })
);

// Passport Initialization
app.use(passport.initialize());
app.use(passport.session());

// Express Validator
app.use(
  expressValidator({
    errorFormatter: function(param, msg, value) {
      var namespace = param.split("."),
        root = namespace.shift(),
        formParam = root;

      while (namespace.length) {
        formParam += "[" + namespace.shift() + "]";
      }
      return {
        param: formParam,
        msg: msg,
        value: value
      };
    }
  })
);

// Connect Flash
app.use(flash());

// Global Vars
app.use(function(req, res, next) {
  res.locals.success_msg = req.flash("success_msg");
  res.locals.error_msg = req.flash("error_msg");
  res.locals.error = req.flash("error");
  res.locals.user = req.user || null;
  next();
});

app.use("/", indexRouter);
app.use("/users", usersRouter, forgotPasswordRouter);
app.use("/api", sensorsRouter, graphsRouter, zoneRouter, mapRouter);
app.use("/device", detailsRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

const io = require("socket.io")(server);

// mqtt
var client = mqtt.connect("mqtt://broker.hivemq.com");

var connections = [];

client.subscribe("Digital_box");

console.log("Subscribed to Digital_box");

client.on("connect", function() {
  client.subscribe("Digital_box");
  console.log("Subscribed to Digital_box");
});

let datetime, month, date;
client.on("message", function(topic, message) {
  //Moment Starts
  month = moment().month() + 1;
  date = moment().date();
  if (date < 10) {
    date = "0" + date;
  }
  if (month < 10) {
    month = "0" + month;
  }
  datetime =
    moment().year() +
    "-" +
    month +
    "-" +
    date +
    " " +
    moment().hours() +
    ":" +
    moment().minutes() +
    ":" +
    moment().seconds();
  console.log("Time Show: " + datetime);
  //Moment Ends

  if (topic == "Digital_box") {
    console.log(topic + ": " + message);

    // Handle data error
    if (message) {
      try {
        var value = JSON.parse(message);
        if (
          value.TMP < 5 ||
          value.TMP > 50 ||
          value.HUM < 30 ||
          value.HUM > 95 ||
          value.CO2 < 400 ||
          value.CO2 > 8192 ||
          value.VOC < 0 ||
          value.VOC > 1188 ||
          value.CH4 < 0 ||
          value.CH4 > 10000
        ) {
          dbo.outliers.insert({
            DATETIME: datetime,
            TEMPERATURE: value.TMP,
            HUMIDITY: value.HUM,
            CO2: value.CO2,
            VOC: value.VOC,
            CH4: value.CH4
          });
        } else {
          dbs.sensordata.insert({
            DATETIME: datetime,
            TEMPERATURE: value.TMP,
            HUMIDITY: value.HUM,
            CO2: value.CO2,
            VOC: value.VOC,
            CH4: value.CH4
          });
          tsclient.attachChannel(236908, { writeKey: "RKEAPCCKRUM3QUQL" });
          tsclient.updateChannel(236908, {
            field1: value.TMP,
            field2: value.HUM,
            field3: value.CO2,
            field4: value.VOC,
            field5: value.CH4
          });
        }
      } catch (e) {
        console.log("Wrong Data Format");
      }
    }
  }
});

// Socket starts here
io.on("connection", function(socket) {
  connections.push(socket);
  console.log("A user connected");
  console.log("Connected\n" + connections.length + " clients connected");
  socket.on("disconnect", function() {
    connections.splice(connections.indexOf(socket), 1);
    console.log("A user disconnected");
    console.log("Connected\n" + connections.length + " clients connected");
  });
  client.on("message", function(topic, message) {
    io.sockets.emit("mqtt", { topic: String(topic), payload: String(message) });
  });
});

server.listen(process.env.PORT || 3000);
