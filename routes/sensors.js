var express = require("express");
var mongojs = require("mongojs");
var ThingSpeakClient = require("thingspeakclient");
var tsclient = new ThingSpeakClient();

var router = express.Router();

var db = mongojs("mongodb://localhost:27017/digitalbox", ["sensordata"]);

// Ensure Authentication
function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  } else {
    req.flash("error_msg", "You are not logged in");
    res.redirect("/users/login");
  }
}

// Get All Data
router.get("/sensors", ensureAuthenticated, function(req, res, next) {
  db.sensordata.find(function(err, sensordata) {
    if (err) {
      res.send(err);
    }
    // res.json(sensordata);
    res.render("sensor", {
      data: sensordata
    });
  });
});

// Get All Data from Android
router.get("/appsensordata", function(req, res, next) {
  db.sensordata.find(function(err, sensordata) {
    if (err) {
      res.send(err);
    }
    res.json(sensordata);
  });
});

// Get Single Data from List
router.put("/sensor/:id/edit", ensureAuthenticated, function(req, res, next) {
  db.sensordata.findOne({ _id: mongojs.ObjectId(req.params.id) }, function(
    err,
    sensor
  ) {
    if (err) {
      res.send(err);
    }
    res.render("edit", {
      data: sensor
    });
  });
});

// Save Sensor Data
// router.post('/sensors', function(req, res, next){
//     var sensor = req.body;
//     console.log("Data is here");
//     db.sensordata.save(sensor, function(){
//         if(err){
//             res.send(err);
//         }
//         res.json(sensor);
//     });
// });

// Update sensor data
router.post("/update", function(req, res) {
  console.log("Update is here");
  var _id = req.body._id;
  var datetime = req.body.DATETIME;
  var temperature = req.body.TEMPERATURE;
  var humidity = req.body.HUMIDITY;
  var co2 = req.body.CO2;
  var voc = req.body.VOC;
  var ch4 = req.body.CH4;
  db.sensordata.update(
    {
      _id: mongojs.ObjectId(_id)
    },
    {
      $set: {
        DATETIME: datetime,
        TEMPERATURE: temperature,
        HUMIDITY: humidity,
        CO2: co2,
        VOC: voc,
        CH4: ch4
      }
    },
    function(err) {
      if (err) {
        res.send(err);
      }
      res.redirect("/device/detail/" + _id);
    }
  );
});

// Delete Sensor Data
router.delete("/sensor/:id/delete", ensureAuthenticated, function(
  req,
  res,
  next
) {
  db.sensordata.remove({ _id: mongojs.ObjectId(req.params.id) }, function(
    err,
    sensor
  ) {
    if (err) {
      res.send(err);
    }
    res.redirect("/api/sensors");
  });
});

module.exports = router;
