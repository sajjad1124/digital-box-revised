var express = require("express");
var router = express.Router();

// Get Area View
router.get("/zone", ensureAuthenticated, function(req, res, next) {
  res.render("zone");
});

// Ensure Authentication
function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  } else {
    res.redirect("/users/login");
  }
}

module.exports = router;
