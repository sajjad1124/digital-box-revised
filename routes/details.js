var express = require("express");
var mongojs = require("mongojs");
var db = mongojs("mongodb://localhost/digitalbox", ["sensordata"]);

var router = express.Router();

//Get Data Details
router.get("/detail/:id", ensureAuthenticated, function(req, res) {
  db.sensordata.findOne({ _id: mongojs.ObjectId(req.params.id) }, function(
    err,
    dataDetail
  ) {
    if (err) {
      res.send(err);
    }
    res.render("detail", {
      data: dataDetail
    });
  });
  // res.render('detail');
});

//Ensure Authentication
function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  } else {
    res.redirect("/users/login");
  }
}

module.exports = router;
