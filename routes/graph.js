var express = require('express');
var router = express.Router();

//Get Temperature Graph
router.get('/plotly', ensureAuthenticated, function(req, res){
    res.render('plotly');
});


// Get Thingspeak
router.get('/graphs',ensureAuthenticated, function(req, res){
    res.render('graph');
});

// Ensure Authentication
function ensureAuthenticated(req, res, next){
    if(req.isAuthenticated()){
        return next();
    } else {
        res.redirect('/users/login');
    }
}

module.exports = router;
