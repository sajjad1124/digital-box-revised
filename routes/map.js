var express = require('express');
var router = express.Router();

// Get Map View
router.get('/map', ensureAuthenticated, function(req, res){
    res.render('map');
});

// Ensure Authentication
function ensureAuthenticated(req, res, next){
    if(req.isAuthenticated()){
        return next();
    } else {
        res.redirect('/users/login');
    }
}

module.exports = router;