var express = require("express");
var mongojs = require("mongojs");
var router = express.Router();

var db = mongojs("mongodb://localhost:27017/digitalbox", ["sensordata"]);

// Get Homepage
router.get("/", ensureAuthenticated, function(req, res) {
  db.sensordata.findOne({}, function(err, lastData) {
    if (err) {
      res.send(err);
    }
    res.render("index", {
      data: lastData
    });
  });
});

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  } else {
    res.redirect("/users/login");
  }
}

module.exports = router;
