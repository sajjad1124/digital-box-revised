var path = require("path");
var express = require("express");
var router = express.Router();
var nodemailer = require("nodemailer");
var mongojs = require("mongojs");
var db = mongojs("mongodb://localhost/digitalbox", ["users"]);

// Require email as input
router.get("/forgotPassword", function(req, res) {
  db.users.find(function(err, docs) {
    if (err) {
      res.send(err);
    }
    res.render("forgotPassword", {
      data: docs
    });
  });
  // res.render('forgotPassword');
});

router.post("/sendmail", function(req, res) {
  var email = req.body.email;

  // Validation
  req.checkBody("email", "Email is required").notEmpty();

  var errors = req.validationErrors();
  if (errors) {
    res.render("forgotPassword", {
      errors: errors
    });
  } else {
    // Search mail
    db.users.find({ email: req.body.email }, function(err, docs) {
      if (err) {
        console.log(err);
      }

      if (docs.length === 0) {
        res.render("forgotPassword", {
          errors: "Unregistered email"
        });
      } else {
        // Execute plan

        console.log("Sending to" + req.body.email);
        console.log("Docs: " + docs);
        // var obj = JSON.parse(docs);
        var name = "Sajjad";
        var password = "jarvis";
        var transporter = nodemailer.createTransport({
          service: "gmail",
          secure: false,
          port: 25,
          auth: {
            user: "dblackboxbd@gmail.com",
            pass: "gojarvis"
          },
          tls: {
            rejectUnauthorized: false
          }
        });
        var HelperOptions = {
          from: '"Muhammad Sajjad Hussein" <dblackboxbd@gmail.com>',
          to: req.body.email,
          subject: "Password Recovery",
          text: `Hello ${docs.name}, Your password is ${docs.password}`
        };
        transporter.sendMail(HelperOptions, (error, info) => {
          if (error) {
            console.log(error);
          }
          console.log("The message was sent.");
        });
        res.redirect("/users/login");
        //Plan Ends
      }
    });
  }
});

module.exports = router;
